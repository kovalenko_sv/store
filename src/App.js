import React, { useEffect } from "react";
import { useState } from "react";
import Header from "./components/Header";
import Items from "./components/Items";

function App() {
  const [items, setItems] = useState([]);
  const [favorites, setFav] = useState(
    JSON.parse(localStorage.getItem("favoriteList")) || []
  );
  const [orders, setOrder] = useState([]);

  useEffect(() => {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => setItems(data));
  }, []);

  const addToOrder = (item) => {
    let newList;

    if (orders.includes(item)) {
      return;
    } else {
      newList = [...orders, item];
    }
    setOrder(newList);
    localStorage.setItem("productItem", JSON.stringify(newList));
  };

  const deleteOrder = (id) => {
    const newCart = orders.filter((el) => el.id !== id);
    setOrder(newCart);
    localStorage.setItem("productItem", JSON.stringify(newCart));
  };

  const addToFav = (item) => {
    let resultArr;
    if (favorites.includes(item.id)) {
      resultArr = favorites.filter((el) => el !== item.id);
    } else {
      resultArr = [...favorites, item.id];
    }
    setFav(resultArr);
    localStorage.setItem("favoriteList", JSON.stringify(resultArr));
  };

  return (
    <div className="App">
      <Header orders={orders} onDelete={deleteOrder} />
      <Items
        items={items}
        onAdd={addToOrder}
        onFav={addToFav}
        favList={favorites}
      />
    </div>
  );
}

export default App;
