import React from "react";
import { FaTrash } from "react-icons/fa";
import PropTypes from "prop-types";

export default class Order extends React.Component {
  render() {
    return (
      <div className="item">
        <img src={"./img" + this.props.item.img} width="200" height="200" />
        <h2>{this.props.item.title}</h2>
        <p>price: {this.props.item.price}$</p>
        <FaTrash
          className="delete-icon"
          onClick={() => this.props.onDelete(this.props.item.id)}
        />
      </div>
    );
  }
}

Order.propTypes = {
  img: PropTypes.string,
  titile: PropTypes.string,
  price: PropTypes.string,
  id: PropTypes.number,
};
Order.defaultProps = {
  titile: "Title",
  price: "Price",
};
