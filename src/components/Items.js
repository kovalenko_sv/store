import React from "react";
import Item from "./Item";
import PropTypes from "prop-types";

export default class Items extends React.Component {
  render() {
    return (
      <main>
        {this.props.items.map((el) => (
          <Item
            key={el.id}
            item={el}
            onAdd={this.props.onAdd}
            onFav={this.props.onFav}
            isFav={this.props.favList.includes(el.id)}
          />
        ))}
      </main>
    );
  }
}

Items.propTypes = {
  id: PropTypes.number,
  onAdd: PropTypes.func,
  onFav: PropTypes.func,
};
