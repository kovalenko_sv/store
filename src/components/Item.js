import React from "react";
import { Button } from "./Button";
import { Modal, Actions } from "./Modal";
import { Star } from "./icon/index";
import PropTypes from "prop-types";

export default class Item extends React.Component {
  state = {
    modal1: false,
    modal2: false,
  };
  render() {
    return (
      <div className="item">
        <img src={"./img" + this.props.item.img} width="200" height="200" />
        <h2>{this.props.item.title}</h2>
        <p>article: {this.props.item.article}</p>
        <p>price: {this.props.item.price}$</p>
        <p>color: {this.props.item.color}</p>
        <div>
          <Button
            style={{
              backgroundColor: "#0306f6",
              padding: "5px",
              color: "white",
              border: "none",
              borderRadius: "5px",
            }}
            className="first-btn"
            onClick={() =>
              this.setState({
                modal1: true,
              })
            }
            text="Add to cart"
          />
          <Modal
            onModalClose={() => this.setState({ modal1: false })}
            closeBtn={this.state.modal1}
            header="Do you want to add this product to your cart?"
            text="You can add this item and continue"
            actions={
              <Actions
                onClick={() => {
                  this.props.onAdd(this.props.item);
                  this.setState({ modal1: false });
                }}
                onCancel={() => {
                  this.setState({ modal1: false });
                }}
                firstBtn="OK"
                secondBtn="CANCEL"
              />
            }
          />
          <button className={`star ${this.props.isFav ? "active" : ""}`}>
            <Star
              onClick={() => {
                this.props.onFav(this.props.item);
              }}
            />
          </button>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  img: PropTypes.string,
  titile: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.string,
  id: PropTypes.number,
};
Item.defaultProps = {
  titile: "Title",
  price: "Price",
  article: "0000000",
  color: "black",
};
